import { mount } from "enzyme";
import { describe, expect } from "@jest/globals";
import { act, wrapExample } from "./utils";
import { useFluidState } from "../index";
import { testCounter } from "../test-counter";

describe("Simple example", () => {
	const example = wrapExample(
		() => ({
			name: "John",
			age: 31,
		}),
		({ state }) => {
			const { name, age } = useFluidState(() => state);
			return (
				<div>
					Hello {name} aged {age}
				</div>
			);
		},
		({ state }) => {
			return (
				<div>
					Hello {state.name} aged {state.age}
				</div>
			);
		}
	);

	example.test(
		"can perform simple updates",
		async (Component, state, getRenderCount) => {
			const instance = mount(<Component state={state} />);
			expect(instance.html()).toEqual(`<div>Hello John aged 31</div>`);
			expect(getRenderCount()).toEqual(1);

			await act(() => {
				state.name = "Cory";
			});
			expect(instance.html()).toEqual(`<div>Hello Cory aged 31</div>`);
			expect(getRenderCount()).toEqual(2);

			await act(() => {
				state.name = "Brian";
				state.age = 45;
			});
			expect(instance.html()).toEqual(`<div>Hello Brian aged 45</div>`);
			expect(getRenderCount()).toEqual(3);

			testCounter.startCount?.();
			await act(() => {
				instance.unmount();
			});
			await act(() => {
				state.name = "George";
				state.age = 29;
			});
			expect(testCounter.stopCount?.()).toEqual(0);
			expect(getRenderCount()).toEqual(3);
		}
	);
});

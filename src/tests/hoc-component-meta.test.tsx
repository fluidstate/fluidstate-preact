import { describe, expect, test } from "@jest/globals";
import { withFluidState } from "../index";
import { FunctionComponent } from "preact";

describe("HOC component metadata", () => {
	test("component metadata is preserved", () => {
		const Component: FunctionComponent<{}> = () => {
			return null;
		};
		Component.defaultProps = {};
		Component.displayName = "MyComponent";
		const FluidComponent = withFluidState(Component);
		expect(Component.defaultProps === FluidComponent.defaultProps).toEqual(
			true
		);
		expect(FluidComponent.displayName).toEqual(`FluidComponent(MyComponent)`);
	});
});

import { mount } from "enzyme";
import { describe, expect } from "@jest/globals";
import { act, wrapExample } from "./utils";
import { useFluidState } from "../index";

describe("Component depending on computed property", () => {
	const example = wrapExample(
		() => ({
			a: 0,
			b: 0,
			get c() {
				return this.a + this.b;
			},
		}),
		({ state }) => {
			const sum = useFluidState(() => state.c);
			return <div>Sum is {sum}</div>;
		},
		(props) => {
			return <div>Sum is {props.state.c}</div>;
		}
	);

	example.test(
		"update is triggered when computed is changed",
		async (Component, state, getRenderCount) => {
			const instance = mount(<Component state={state} />);
			expect(instance.html()).toEqual(`<div>Sum is 0</div>`);
			expect(getRenderCount()).toEqual(1);

			await act(() => {
				state.a = 100;
				state.b = -97;
			});
			expect(instance.html()).toEqual(`<div>Sum is 3</div>`);
			expect(getRenderCount()).toEqual(2);

			await act(() => {
				state.a = 105;
				state.b = -97;
			});
			expect(instance.html()).toEqual(`<div>Sum is 8</div>`);
			expect(getRenderCount()).toEqual(3);
		}
	);

	example.test(
		"update is not triggered when computed is not changed",
		async (Component, state, getRenderCount) => {
			const instance = mount(<Component state={state} />);
			expect(instance.html()).toEqual(`<div>Sum is 0</div>`);
			expect(getRenderCount()).toEqual(1);

			await act(() => {
				state.a = 100;
				state.b = -100;
			});
			expect(instance.html()).toEqual(`<div>Sum is 0</div>`);
			expect(getRenderCount()).toEqual(2);

			await act(() => {
				state.a = 200;
				state.b = -200;
			});
			expect(instance.html()).toEqual(`<div>Sum is 0</div>`);
			expect(getRenderCount()).toEqual(3);
		}
	);
});

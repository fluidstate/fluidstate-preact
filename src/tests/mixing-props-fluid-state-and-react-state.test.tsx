import { useState } from "preact/hooks";
import { mount } from "enzyme";
import { describe, expect } from "@jest/globals";
import { act, wrapExample } from "./utils";
import { useFluidState } from "../index";

type FluidState = {
	name: string;
};

type Props = {
	age: number;
	state: FluidState;
};

type ReactState = {
	isMarried: boolean;
};

describe("Mixing props, Fluid State and Preact state", () => {
	const example = wrapExample<FluidState, Props>(
		() => ({
			name: "John",
		}),
		({ state, age }) => {
			const { name } = useFluidState(() => state);
			const [reactState, setReactState] = useState<ReactState>(() => ({
				isMarried: false,
			}));
			return (
				<div>
					Hello {name} - your age is {age} - you are{" "}
					{!reactState.isMarried ? "not" : ""} married.
					<button
						id="marriage-toggle"
						onClick={() =>
							setReactState((state) => ({
								...state,
								isMarried: !state.isMarried,
							}))
						}
					>
						Toggle marriage status
					</button>
				</div>
			);
		},
		({ state, age }) => {
			const [reactState, setReactState] = useState<ReactState>(() => ({
				isMarried: false,
			}));
			return (
				<div>
					Hello {state.name} - your age is {age} - you are{" "}
					{!reactState.isMarried ? "not" : ""} married.
					<button
						id="marriage-toggle"
						onClick={() =>
							setReactState((state) => ({
								...state,
								isMarried: !state.isMarried,
							}))
						}
					>
						Toggle marriage status
					</button>
				</div>
			);
		}
	);

	example.test(
		"can perform updates on all things",
		async (Component, state, getRenderCount) => {
			const instance = mount(<Component state={state} age={35} />);
			expect(instance.html()).toEqual(
				`<div>` +
					`Hello John - your age is 35 - you are not married.` +
					`<button id="marriage-toggle">Toggle marriage status</button>` +
					`</div>`
			);
			expect(getRenderCount()).toEqual(1);

			await act(() => {
				instance.setProps({ state, age: 36 });
			});
			expect(instance.html()).toEqual(
				`<div>` +
					`Hello John - your age is 36 - you are not married.` +
					`<button id="marriage-toggle">Toggle marriage status</button>` +
					`</div>`
			);
			expect(getRenderCount()).toEqual(2);

			await act(() => {
				instance.find({ id: "marriage-toggle" }).simulate("click");
			});
			expect(instance.html()).toEqual(
				`<div>` +
					`Hello John - your age is 36 - you are  married.` +
					`<button id="marriage-toggle">Toggle marriage status</button>` +
					`</div>`
			);
			expect(getRenderCount()).toEqual(3);

			await act(() => {
				state.name = "Mary";
			});
			expect(instance.html()).toEqual(
				`<div>` +
					`Hello Mary - your age is 36 - you are  married.` +
					`<button id="marriage-toggle">Toggle marriage status</button>` +
					`</div>`
			);
			expect(getRenderCount()).toEqual(4);
		}
	);
});

import { FunctionComponent } from "preact";
import { useEffect, useRef, useState } from "preact/hooks";
import { testCounter } from "./test-counter";
import { useOnceRef } from "./once-ref";
import {
	createComputed,
	createCoreReaction,
	createObserved,
	disableComputed,
	get,
	Node,
	notify,
} from "fluidstate/core";
import { triggerUpdate } from "fluidstate";

export const withFluidState = <P>(Component: FunctionComponent<P>) => {
	const FluidComponent: FunctionComponent<P> = (...args) => {
		const argsRef = useRef(args);
		argsRef.current = args;

		const data = useOnceRef<{
			isInitialized: boolean;
			isWithinRender: boolean;
			result: ReturnType<FunctionComponent<P>>;
		}>(() => ({
			isInitialized: false,
			isWithinRender: false,
			result: null,
		}));

		const [, rerender] = useState(Symbol());

		const renderControl = useOnceRef(() => createObserved(null));

		// We should only rerun component render within rendering context
		// and in a way that preserves hook order
		const effect = useOnceRef(() => {
			let dependencies = new Set<Node<unknown>>();

			const computed = createComputed(() => {
				// This subscribes to renderControl, which allows us
				// to rerun computed at the right time
				get(renderControl.current);

				if (data.current.isWithinRender) {
					data.current.result = Component(...argsRef.current);
					dependencies = new Set(computed.dependencies);
				} else {
					// Getting dependencies to preserve the same dependency list
					for (const dependency of dependencies) {
						get(dependency);
					}
					// When not within render context, we shouldn't rerun
					// Component render. Instead, we simply ask React to rerender
					if (data.current.isInitialized) {
						return true;
					} else {
						data.current.isInitialized = true;
					}
				}

				return false;
			});

			const stopReaction = createCoreReaction(
				() => get(computed),
				(shouldRerender) => {
					if (shouldRerender) {
						rerender(Symbol());
						// @ts-ignore
						if (__DEV__ && testCounter.increaseCount) {
							testCounter.increaseCount();
						}
					}
				}
			);

			return { computed, stopReaction };
		});

		useEffect(() => {
			return () => {
				const { computed, stopReaction } = effect.current;
				disableComputed(computed);
				stopReaction();
			};
		}, []);

		// At this point, we should rerender Component, since we are in
		// the rendering context
		data.current.isWithinRender = true;
		notify(renderControl.current);
		triggerUpdate();
		data.current.isWithinRender = false;

		return data.current.result;
	};

	if (Component.defaultProps) {
		FluidComponent.defaultProps = Component.defaultProps;
	}

	if (Component.displayName || Component.name) {
		FluidComponent.displayName = `FluidComponent(${
			Component.displayName || Component.name
		})`;
	}

	return FluidComponent;
};
